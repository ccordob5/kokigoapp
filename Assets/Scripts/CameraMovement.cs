﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    [SerializeField]
    private Camera _playerCamera;
    [SerializeField]
    private float _rotationSpeed = 5;
    [SerializeField]
    private float _zoomSpeed = 5;
    [SerializeField]
    private Vector2 _FOVMinMax;

    [Header("Configuracion Zoom")]
    [SerializeField]
    private Vector3 _posZoomMax;
    [SerializeField]
    private Vector3 _posZoomMin;
    [SerializeField]
    private Vector3 _rotationZoomMax;
    [SerializeField]
    private Vector3 _roatiationZoomMin;
    [SerializeField]
    private float _factorCambio = 0.01f;

    [Header("Interacciones")]
    [SerializeField]
    private LayerMask _creatureLayer;
    [SerializeField]
    private float _maxRayDistance = 75;

    private Vector3 _lastPosition  =Vector3.zero;
    private float _previousDistance = 0;

    [Range(0,1)]
    private float _currentZoomDist;

    private float _currentTouchTime = 0;
    private float _touchTimeToMove = 1.5f;

    // Start is called before the first frame update
    void Start() {
        if(_playerCamera == null) {
            _playerCamera = Camera.main;
            //_playerCamera = transform.GetChild(0).GetComponent<Camera>();
		}
    }

    // Update is called once per frame
    void Update() {
		if (Input.touchCount > 0) {

            if (Input.touchCount == 2) {
                Touch finger1 = Input.GetTouch(0);
                Touch finger2 = Input.GetTouch(1);

                CameraZoom(finger1.position, finger2.position);
                return;
            }

            if (Input.touchCount == 1) {
                Touch playerTouch = Input.GetTouch(0);
                if(playerTouch.phase == TouchPhase.Moved) {
                    RotateCamera(playerTouch.position);
                    _currentZoomDist = 0;
                }
                
                if(playerTouch.phase == TouchPhase.Began) {
                    AnalizarPosicion(playerTouch);
                }
                return;
            }

            
		} else {
            _currentZoomDist = 0;
		}

        _currentTouchTime += Time.deltaTime;
        if(_currentTouchTime >= _touchTimeToMove) {

		}
    }

	void AnalizarPosicion(Touch playerTouch) {
        //Ubicar posicion tocada a posicion en el mundo
        //Vector3 mousePosition = Camera.main.ViewportToWorldPoint(playerTouch.position);

        //Usando layer, crear el raycast
        RaycastHit hit;
        Ray camRay = Camera.main.ScreenPointToRay(playerTouch.position);
		if (Physics.Raycast(camRay, out hit, _maxRayDistance,_creatureLayer)) {
            //Debug.Log(hit.transform.gameObject);
            hit.transform.SetParent(null);
            SceneChangeManagement.Instance.CargarEsceneaConCriatura(hit.transform.gameObject);

            //Jugador.Instance.AgregarCriaturaAInventario(hit.transform.GetComponent<Criatura>().Id, hit.transform.name);

		}
	}

    void RotateCamera(Vector3 currentPos) {
        float x = currentPos.x;
        float difference = x - _lastPosition.x;
        if (difference > 0) {
            Vector3 rotationVector = Vector3.up * _rotationSpeed;
            transform.Rotate(rotationVector);
            //Quaternion rotation = _playerCamera.transform.localRotation;
            //_playerCamera.transform.localRotation =
        } else if (difference < 0) {
            Vector3 rotationVector = Vector3.down * _rotationSpeed;
            transform.Rotate(rotationVector);
        }

        //Reemplazamos vector pasado a el vector actual
        _lastPosition = currentPos;
    }

    void CameraZoom(Vector3 posFinger1, Vector3 posFinger2 ) {
        float currentDistance = Vector3.Distance(posFinger1, posFinger2);

        float distance = currentDistance - _previousDistance;

        if((currentDistance - _previousDistance) > 0) {
            //Alejando
            //Vector3.Lerp(_posZoomMin, _posZoomMax, 0 - 1);

            _currentZoomDist += _factorCambio * _zoomSpeed;
            _currentZoomDist = Mathf.Clamp(_currentZoomDist, 0, 1);

            Vector3 currentCamPos = _playerCamera.transform.position;
            currentCamPos = Vector3.Lerp(currentCamPos, _posZoomMax, _currentZoomDist);
            Vector3 currentCamRot = _playerCamera.transform.rotation.eulerAngles;
            currentCamRot = Vector3.Lerp(currentCamRot, _rotationZoomMax, _currentZoomDist);
            //currentCamRot.y = 0;

            _playerCamera.transform.position = currentCamPos;
            if(_currentZoomDist == 1) {

			} else {
                _playerCamera.transform.rotation = Quaternion.Euler(currentCamRot);
            }
            

            //float currentFoV = Camera.main.fieldOfView;
            //currentFoV += 0.1f * _zoomSpeed;
            //currentFoV = Mathf.Clamp(currentFoV, _FOVMinMax.x, _FOVMinMax.y);
            //_playerCamera.fieldOfView = currentFoV;
		}else if((currentDistance - _previousDistance) < 0) {
            //Acercando
            //Debug.Log("Zooming out");
            //float currentFoV = _playerCamera.fieldOfView;
            //currentFoV -= 0.1f * _zoomSpeed;
            //currentFoV = Mathf.Clamp(currentFoV, _FOVMinMax.x, _FOVMinMax.y);
            //_playerCamera.fieldOfView = currentFoV;

            _currentZoomDist += _factorCambio * _zoomSpeed;
            _currentZoomDist = Mathf.Clamp(_currentZoomDist, 0, 1);

            Vector3 currentCamPos = _playerCamera.transform.position;
            currentCamPos = Vector3.Lerp(currentCamPos, _posZoomMin, _currentZoomDist);
            Vector3 currentCamRot = _playerCamera.transform.rotation.eulerAngles;
            currentCamRot = Vector3.Lerp(currentCamRot, _roatiationZoomMin, _currentZoomDist);
            //currentCamRot.y = 0;

            _playerCamera.transform.position = currentCamPos;
            _playerCamera.transform.rotation = Quaternion.Euler(currentCamRot);
		} else {
            _currentZoomDist = 0;
		}

        _previousDistance = currentDistance;
	}
}
