﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Criatura : MonoBehaviour {

    [SerializeField]
    private int id;
    [SerializeField]
    private float _MaxDissapearingDistance = 15;
    [SerializeField]
    private float _posibilidadDeCaptura = .75f;

    private Transform _player;

	public int Id { get => id; }

	// Start is called before the first frame update
	void Start() {
        _player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update() {
        if(SceneChangeManagement.Instance.CurrentScene == 0) {
            CheckDistanceToPlayer();
        }
    }

    void CheckDistanceToPlayer() {
        if(_player != null) {
            float distance = Vector3.Distance(_player.position, transform.position);
            if (distance > _MaxDissapearingDistance) {
                CriaturasManager.Instance.RemoveFromList(gameObject);
                Destroy(gameObject);
            }
        }
	}

	private void OnCollisionEnter( Collision collision ) {
		if (collision.gameObject.CompareTag("Criatura")) {
            if (Random.value >= _posibilidadDeCaptura) {
                //Capturelo
            } else {
                //Volver a ponerlo en la escena
            }


        }
		
	}
}
