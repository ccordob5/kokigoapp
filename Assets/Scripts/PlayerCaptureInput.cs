﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCaptureInput : MonoBehaviour
{

    [SerializeField]
    private float _maxTouchDistance = 10;
    [SerializeField]
    private LayerMask _plateLayer;

    private bool _isTouchingPlate = false;
    private Plate _heldPlate;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.touchCount > 0) {
            Touch playerTouch = Input.GetTouch(0);
            if(playerTouch.phase == TouchPhase.Began) {
                CheckForPlateTouch(playerTouch);
			}

            if(playerTouch.phase == TouchPhase.Moved) {
                MovePlateWhenTouched(playerTouch);
			}

            if(playerTouch.phase == TouchPhase.Ended) {
                CheckForPlateRelease();
			}
		}
    }

    void CheckForPlateTouch( Touch playerTouch ) {
        //Vector3 mousePosition = Camera.main.ViewportToWorldPoint(playerTouch.position);

        RaycastHit hit;
        Ray camRay = Camera.main.ScreenPointToRay(playerTouch.position);
        if (Physics.Raycast(camRay, out hit, _maxTouchDistance, _plateLayer)) {
            //Debug.Log("Tocuhed Plate!");
            _isTouchingPlate = true;
            _heldPlate = hit.transform.gameObject.GetComponent<Plate>();
        }
    }

    void CheckForPlateRelease() {
		if (_isTouchingPlate) {
            _isTouchingPlate = false;
            _heldPlate.ThrowPlate();
            Debug.Log("Solte el Plato");
		}
	}

    void MovePlateWhenTouched(Touch playerTouch) {
		if (_isTouchingPlate) {
            //Vector3 plateDesiredPosition = Camera.main.ScreenToWorldPoint(playerTouch.position);
            //Debug.Log(playerTouch.position.ToString());
            //_heldPlate.ThrowPlate();
		}
	}
}
