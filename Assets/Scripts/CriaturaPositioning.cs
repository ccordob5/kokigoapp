﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriaturaPositioning : MonoBehaviour
{
    [SerializeField]
    private Transform _parentObject;
    [SerializeField]
    private float _tiempoDeEspera = 0.5f;
    // Start is called before the first frame update
    void OnEnable()
    {
        StartCoroutine(SetParentGO());
    }

    IEnumerator SetParentGO() {
        yield return new WaitForSeconds(_tiempoDeEspera);
        SceneChangeManagement.Instance.CriaturaParaEscena.transform.SetParent(_parentObject);
    }
}
