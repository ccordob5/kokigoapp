﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour
{
    public static Jugador Instance; 

    [SerializeField]
    private List<CriaturaCapturada> _inventarioCriaturas = new List<CriaturaCapturada>();

	private void Awake() {
        if (Jugador.Instance != null) {
            if(Jugador.Instance != this) {
                Destroy(this.gameObject);
			}
		} else {
            Jugador.Instance = this;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AgregarCriaturaAInventario(int id, string name) {
        bool criaturaEnInventario = false;
        foreach(CriaturaCapturada cr in _inventarioCriaturas) {
            if(cr.id == id) {
                criaturaEnInventario = true;
                //Realizar accion

                //Checkeo de promocion
                cr.totalCapturado++;
                cr.capturadoParaPromocion++;
                if(cr.capturadoParaPromocion >= cr.necesarioParaPromocion) {
                    cr.capturadoParaPromocion = cr.necesarioParaPromocion;
				}
			}
		}

        CriaturaCapturada criatura = new CriaturaCapturada(id);
        //criatura.id = id;
        criatura.name = name;
        criatura.totalCapturado++;
        _inventarioCriaturas.Add(criatura);

        //Referencia a clase helper de cservidor
        ConnectionManager.Instance.CriaturaCapturada(id);
	}
}
