﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CriaturaCapturada {

    public int id;
    public string name;
    public int totalCapturado;
    public int necesarioParaPromocion = 3;
    public int capturadoParaPromocion;

    //Constructor
    public CriaturaCapturada() {

    }

    //Constructor 2 con id
    public CriaturaCapturada( int id ) {
        id = this.id;
        //Con el ID obtiene el nombre
    }


}
