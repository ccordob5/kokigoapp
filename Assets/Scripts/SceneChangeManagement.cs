﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChangeManagement : MonoBehaviour
{

    public static SceneChangeManagement Instance;

    [SerializeField]
    private GameObject _criaturaParaEscena;

    [SerializeField]
    private string _prototypingScene;
    [SerializeField]
    private string _capturaScene;

    [Header("Posiciones en Captura")]
    [SerializeField]
    private Vector3 _posicionEnCaptura;

    private int _currentScene = 0;

	public int CurrentScene { get => _currentScene; }
	public GameObject CriaturaParaEscena { get => _criaturaParaEscena; }

	private void Awake() {
		if(SceneChangeManagement.Instance != null) {
            if(SceneChangeManagement.Instance != this) {
                Destroy(this.gameObject);
			}
		} else {
            SceneChangeManagement.Instance = this;
		}

        DontDestroyOnLoad(this);
	}

	void OnEnable() {
   //     SceneManager.sceneLoaded += ( newScene, mode ) => {
   //         if(newScene.name == "Captura") {
   //             _criaturaParaEscena.transform.SetParent(Camera.main.transform);
			//}
   //     };
	}

	void OnDisable() {
		
	}

	// Start is called before the first frame update
	void Start()
    {
        _criaturaParaEscena = null;
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.L)) {
            if(SceneManager.GetActiveScene().name == "Protipado") {
                SceneManager.LoadScene(1);
			}else if(SceneManager.GetActiveScene().name == "Captura") {
                SceneManager.LoadScene(0);
			}
		}
    }

    public void CargarEsceneaConCriatura(GameObject criatura) {
        if(criatura == null) {
            Destroy(_criaturaParaEscena);
		}
        _criaturaParaEscena = criatura;
		//if (_criaturaParaEscena != null) {
  //          //DontDestroyOnLoad(_criaturaParaEscena);
  //          //SceneManager.MoveGameObjectToScene(_criaturaParaEscena, SceneManager.GetSceneAt(1));
  //      }

		StartCoroutine(CargarEscenaAsincranica());
	}

    IEnumerator CargarEscenaAsincranica() {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = SceneManager.GetActiveScene().name;

        AsyncOperation levelLoad = SceneManager.LoadSceneAsync(sceneName == "Protipado" ?
            _capturaScene : _prototypingScene , LoadSceneMode.Additive );

        //0 -> 1 , 0.1...  donde 1 es que la escena esta cargada
        while (levelLoad.progress < 1) {
            //hacer algo con el nivel de carga(progress)

            yield return null;
        }

        if (_criaturaParaEscena != null) {
            _criaturaParaEscena.transform.SetParent(null);
            _criaturaParaEscena.transform.position = _posicionEnCaptura;
            SceneManager.MoveGameObjectToScene(_criaturaParaEscena, SceneManager.GetSceneByName("Captura"));
            //_criaturaParaEscena.transform.SetParent(Camera.main.transform);
        }

        SceneManager.UnloadSceneAsync(currentScene);

        _currentScene = sceneName == "Protipado" ? 1 : 0;      
    }
}
