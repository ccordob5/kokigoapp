﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateManager : MonoBehaviour {

    public static PlateManager Instance;

    [SerializeField]
    private GameObject _prefabPlato;
    [SerializeField]
    private Vector3 _posicionPlato;
    [SerializeField]
    private Transform _platoParent;

    private Plate _platoActivo;

	public void Awake() {
		if(PlateManager.Instance != null) {
            if(PlateManager.Instance != this) {
                Destroy(this);
			}
		} else {
            PlateManager.Instance = this;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        CrearPlato();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CrearPlato() {
        if(_platoActivo == null) {
            GameObject plato = Instantiate(_prefabPlato, _platoParent);
            plato.transform.localPosition = _posicionPlato;
            _platoActivo = plato.GetComponent<Plate>();
        }
	}

    public void DestruirPlato() {
        Destroy(_platoActivo.gameObject);
        _platoActivo = null;

        CrearPlato();
	}
}
