﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Plate : MonoBehaviour {

	[SerializeField]
	private float _throwForce = 15;
	[SerializeField]
	private float _tiempoDesrtuccion = 1.5f;

	private bool _touchedSomething;

	private Rigidbody _plateRigidBody;
	
	void Start() {
		_plateRigidBody = GetComponent<Rigidbody>();
	}

	public void ThrowPlate() {
		Vector3 forwardDirection = transform.forward;
		forwardDirection = forwardDirection * _throwForce;

		_plateRigidBody.useGravity = true;
		_plateRigidBody.AddForce(forwardDirection,ForceMode.Impulse);
	}

	private void OnCollisionEnter( Collision collision ) {
		if (!_touchedSomething){
			if (collision.gameObject.CompareTag("Criatura")) {
				Debug.Log("Toque Criatura");
				//_plateRigidBody.isKinematic = true;
			}

			_touchedSomething = true;
			StartCoroutine(DestruirPlato());
		}
		
	}

	IEnumerator DestruirPlato() {
		yield return new WaitForSeconds(_tiempoDesrtuccion);
		PlateManager.Instance.DestruirPlato();
	}
}
