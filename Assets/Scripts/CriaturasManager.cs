﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CriaturasManager : MonoBehaviour {

    public static CriaturasManager Instance;

    [SerializeField]
    private List<GameObject> _criaturas = new List<GameObject>();
    [SerializeField]
    private List<GameObject> _criaturasActivas = new List<GameObject>();

    [SerializeField]
    private Transform _player;
    [SerializeField]
    private float _maxRadious = 40;

    [SerializeField]
    private int _maxCriaturas = 5;

    private Transform _criaturasHolder;

	void Awake() {
	    if(CriaturasManager.Instance != null) {
            if(CriaturasManager.Instance != this) {
                Destroy(this);
			}
		} else {
            CriaturasManager.Instance = this;
		}
	}

	// Start is called before the first frame update
	void Start() {
        _criaturasHolder = transform.GetChild(0);

        if(_player == null) {
            _player = GameObject.FindGameObjectWithTag("Player").transform;
		}


        InicializarCiaturasInicio();
    }

    void InicializarCiaturasInicio() {
        for(int i = 0; i < _maxCriaturas; i++) {
            UbicarCriatura();
		}
	}

    void UbicarCriatura() {
        GameObject criatura = Instantiate(_criaturas[Random.Range(0, _criaturas.Count)], _criaturasHolder);

        //Ponerla en una posicion en un radio de 40 m al rededor del usuario.
        //Saber posicion de usuario
        Vector3 playerPos = _player.transform.position;

        //Crear una posicion que se encuentr en un circulo al rededor de la posicion del usuario
        Vector2 randomPos = Random.insideUnitCircle * _maxRadious;
        Vector3 criaturaPos = new Vector3(playerPos.x + randomPos.x, playerPos.y, playerPos.z + randomPos.y);

        //Asignarsela al objeto que creamos
        criatura.transform.position = criaturaPos;

        //Agregar criatura a lista de criaturas activas
        _criaturasActivas.Add(criatura);
	}

    // Update is called once per frame
    void Update() {
        if(_criaturasActivas.Count < _maxCriaturas) {
            UbicarCriatura();
		}
    }

    public void RemoveFromList(GameObject criatura) {
        GameObject toDelete = criatura;
        bool found = false;
        foreach(GameObject go in _criaturasActivas) {
            if (go.transform.position == criatura.transform.position &&
                go.transform.rotation == criatura.transform.rotation) {
                toDelete = go;
                found = true;
                break;
			}
		}

		if (found) {
            _criaturasActivas.Remove(toDelete);
		}
	}
}
