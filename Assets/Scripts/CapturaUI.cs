﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CapturaUI : MonoBehaviour
{

    [SerializeField]
    private Button _menuButton;


    // Start is called before the first frame update
    void Start()
    {
        _menuButton.onClick.AddListener(LoadMapScene);
    }


    void LoadMapScene() {
        SceneChangeManagement.Instance.CargarEsceneaConCriatura(null);
	}
}
