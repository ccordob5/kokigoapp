﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    public static UIManager Instance;

    [SerializeField]
    private Transform _ListaCriaturasUI;
    [SerializeField]
    private Button _OpenListButton;
    [SerializeField]
    private Button _closeListButton;

	private void Awake() {
		if(UIManager.Instance != null) {
            if(UIManager.Instance != this) {
                Destroy(this);
			}
		} else {
            UIManager.Instance = this;
		}
	}

	// Start is called before the first frame update
	void Start()
    {
        AssignEmptyObjects();
        AssignListeners();
    }

    void AssignEmptyObjects() {
        if (_ListaCriaturasUI == null) {
            _ListaCriaturasUI = transform.GetChild(1);
        }

        if (_OpenListButton == null) {
            _OpenListButton = transform.GetChild(0).GetComponent<Button>();
        }

        if (_closeListButton == null) {
            _closeListButton = transform.GetChild(2).GetComponent<Button>();
        }
    }

    void AssignListeners() {
        _OpenListButton.onClick.AddListener(() => ActivarListaInventario());
        _closeListButton.onClick.AddListener(() => DesactivarListaInventario());
	}

    void ActivarListaInventario() {
        //Activar lista
        _ListaCriaturasUI.gameObject.SetActive(true);
        //Desactivar boton de activar lista
        _OpenListButton.gameObject.SetActive(false);
        //Activar Botton de desactivar lista
        _closeListButton.gameObject.SetActive(true);

        //Acceder al jugador, accera la lista que tenemos, y de esa lista vamos a popular la lista del inventario de forma dinamica
	}

    void DesactivarListaInventario() {
        _ListaCriaturasUI.gameObject.SetActive(false);
        _OpenListButton.gameObject.SetActive(true);
        _closeListButton.gameObject.SetActive(false);
	}
}
